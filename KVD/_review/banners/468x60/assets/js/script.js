/* global TimelineMax, Power4, EB, EBG */

// Broadcast Events shim
// ====================================================================================================
(function () {
	if (typeof window.CustomEvent === 'function') {
		return false;
	}

	function CustomEvent(event, params) {
		params = params || { bubbles: false, cancelable: false, detail: undefined };
		var evt = document.createEvent('CustomEvent');
		evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
		return evt;
	}

	CustomEvent.prototype = window.Event.prototype;
	window.CustomEvent = CustomEvent;
})();

// Timeline
// ====================================================================================================
var timeline = (function MasterTimeline() {
	var tl;
	var win = window;

	function doClickTag() {
		window.open(window.clickTag);
	}

	function initTimeline() {
		document.querySelector('#ad .banner').style.display = 'block';
		document.getElementById('ad').addEventListener('click', doClickTag);
		createTimeline();
	}

	function createTimeline() {
		tl = new TimelineMax({
			delay: 0,
			onStart: updateStart,
			onComplete: updateComplete,
			onUpdate: updateStats
		});

		// ---------------------------------------------------------------------------

		tl.add('frame2').to('.js-tag1', 0, { y: '120%', x: '70%', opacity: 0.5, scale: 2.1, ease: Expo.easeInOut }, 'frame2');
		// tl.add('frame3').to('.js-tag2', 0, { y: '120%', x: '62.5%', opacity: 0.5, scale: 2.1, ease: Expo.easeInOut }, 'frame3');
		tl.add('frame3').to('.js-tag3', 0, { y: '100%', x: '75%', opacity: 0.5, scale: 2.1, ease: Expo.easeInOut }, 'frame3');
		tl.add('frame2').to('.js-tag1', 0.3, { y: '30%', ease: Expo.easeInOut }, 'frame2');
		tl.add('frame2').to('.js-tag1', 1.1, { y: '0%', scale: 1, opacity: 1, x: '0%', ease: Expo.easeInOut }, 'frame2');

		// ---------------------------------------------------------------------------

		// tl.add('frame3').to('.js-tag2', 0.3, { y: '30%', ease: Expo.easeInOut }, 'frame3');
		// tl.add('frame3').to('.js-tag2', 1.1, { y: '0%', scale: 1, opacity: 1, x: '0%', ease: Expo.easeInOut }, 'frame3');
		tl.add('frame3').to('.js-tag3', 0.3, { y: '-45%', ease: Expo.easeInOut }, 'frame3');
		tl.add('frame3').to('.js-tag3', 1.1, { y: '0%', scale: 1, opacity: 1, x: '0%', ease: Expo.easeInOut }, 'frame3');

		// ---------------------------------------------------------------------------

		tl
			.add('frame3')
			.from('.js-logo', 0.75, { x: '100%', ease: Expo.easeInOut }, 'frame3')
			.from('.js-cta', 0.75, { x: '100%', ease: Expo.easeInOut }, 'frame3')
			.from('.js-claim', 0.75, { x: '100%', ease: Expo.easeInOut }, 'frame3');

		// ---------------------------------------------------------------------------
	}

	function updateStart() {
		var start = new CustomEvent('start', {
			detail: { hasStarted: true }
		});
		win.dispatchEvent(start);
	}

	function updateComplete() {
		var complete = new CustomEvent('complete', {
			detail: { hasStopped: true }
		});
		win.dispatchEvent(complete);
	}

	function updateStats() {
		var statistics = new CustomEvent('stats', {
			detail: {
				totalTime: tl.totalTime(),
				totalProgress: tl.totalProgress(),
				totalDuration: tl.totalDuration()
			}
		});
		win.dispatchEvent(statistics);
	}

	function getTimeline() {
		return tl;
	}

	return {
		init: initTimeline,
		get: getTimeline
	};
})();

// Banner Init
// ====================================================================================================
timeline.init();
